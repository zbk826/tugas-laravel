@extends('layout.master')

@section('judul')
Halaman Form
@endsection

@section('content')
    <h1>SELAMAT DATANG {{$namefirst}} {{$namelast}} !</h1>
    <h2>Terima kasih telah bergabung di Website Kami. Media Belajar kita bersama.</h2>
@endsection
