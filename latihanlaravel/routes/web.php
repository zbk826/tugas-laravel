<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@home');

Route::get('/register', 'AuthController@register');

Route::post('/welcome', 'AuthController@kirim');

Route::get('/data-tables', function(){
  return view('table.data-tables');
});

// CRUD Cast
// Create
Route::get('/cast/create', 'CastController@create'); // route ke form create
Route::post('/cast', 'CastController@store'); // route untuk menyimpan data ke database

// Read
Route::get('/cast', 'CastController@index'); // route list cast
Route::get('/cast/{cast_id}', 'CastController@show'); // route detail cast

// Update
Route::get('/cast/{cast_id}/edit', 'CastController@edit'); // route menuju form edit
Route::put('/cast/{cast_id}', 'CastController@update'); // route update date berdasarkan id

// Delete
Route::delete('/cast/{cast_id}', 'CastController@destroy'); // route hapus data di database
