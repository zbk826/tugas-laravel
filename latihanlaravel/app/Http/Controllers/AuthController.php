<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
      return view('halaman.register');
    }

    public function kirim(Request $request){
      $namefirst = $request['first'];
      $namelast = $request['last'];

      return view('halaman.welcome', compact('namefirst', 'namelast'));
    }
}
