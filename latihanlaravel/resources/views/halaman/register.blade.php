@extends('layout.master')

@section('judul')
Halaman Form
@endsection

@section('content')
    <h1>Buat Account Baru</h1>
    <form action="/welcome" method="post">
      @csrf
      <label for="">First Name :</label> <br>
      <input type="text" name="first" value=""> <br> <br>
      <label for="">Last Name :</label> <br>
      <input type="text" name="last" value=""> <br> <br>
      <label for="">Gender</label><br> <br>
      <input type="radio" name="gender" value="">Male <br>
      <input type="radio" name="gender" value="">Female <br><br>
      <label for="">Nationality</label><br><br>
      <select class="" name="nationality">
        <option value="indonesia">Indonesia</option>
        <option value="amerika">Amerika</option>
        <option value="Inggris">Inggris</option>
      </select><br><br>
      <label for="">Languange Spoken</label><br><br>
      <input type="checkbox" name="" value="">Bahasa Indonesia <br>
      <input type="checkbox" name="" value="">English <br>
      <input type="checkbox" name="" value="">Other <br>
      <label for="">Bio</label><br><br>
      <textarea name="name" rows="8" cols="80"></textarea><br>
      <input type="submit" name="" value="Sign Up">
    </form>
@endsection
